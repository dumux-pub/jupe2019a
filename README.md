SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

T. Jupe<br>
Numerische Ermittlung der Ergiebigkeit von Brunnen unter Berücksichtigung der Forchheimer-Gleichung und Darstellung der Ergebnisse in dimensionsloser Form
<br>
Master's Thesis, 2019<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir Jupe2019a && cd Jupe2019a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Jupe2019a.git
```

After that, execute the file [installJupe2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Jupe2019a/raw/master/installJupe2019a.sh).

```
./Jupe2019a/installJupe2019a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installJupe2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Jupe2019a/raw/master/installJupe2019a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Jupe2019a
cd Jupe2019a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/jupe2019a/-/raw/master/docker_dumux-brunnen_3d_2p.sh
```

Open the Docker Container
```bash
bash docker_dumux-brunnen_3d_2p.sh open
```

After the script has run successfully, you may build all executables

```bash
cd Jupe2019a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake/forchheimer folder. They can be executed with an input file e.g., by running

```bash
cd forchheimer
./test_1p_forchheimer_tpfa params_forchheimer.input
```
