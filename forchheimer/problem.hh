// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup
 * \brief
 */

#ifndef DUMUX_1PTEST_PROBLEM_HH
#define DUMUX_1PTEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/richards/model.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/flux/forchheimerslaw.hh>

#include "spatialparams.hh"

namespace Dumux {

template <class TypeTag>
class OnePTestProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct OnePTest { using InheritsFrom = std::tuple<Richards>; };
struct OnePTestBox { using InheritsFrom = std::tuple<OnePTest, BoxModel>; };
struct OnePTestCCTpfa { using InheritsFrom = std::tuple<OnePTest, CCTpfaModel>; };
struct OnePTestCCMpfa { using InheritsFrom = std::tuple<OnePTest, CCMpfaModel>; };
} // end namespace TTag

// Specialize the fluid system type for this type tag
//template<class TypeTag>
//struct FluidSystem<TypeTag, TTag::OnePTest>
//{
//    using Scalar = GetPropType<TypeTag, Scalar>;
//    using type = FluidSystems::OnePLiquid<Scalar, Components::SimpleH2O<Scalar> >;
//};

// Specialize the grid type for this type tag
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePTest>
// { using type = Dune::YaspGrid<2>; };
{ using type = Dune::UGGrid<3>; };
// { using type = Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>; };

// Specialize the problem type for this type tag
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePTest>
{ using type = OnePTestProblem<TypeTag>; };

// Specialize the spatial params type for this type tag
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePTest>
{ using type = OnePTestSpatialParams<GetPropType<TypeTag, FVGridGeometry>, GetPropType<TypeTag, Scalar>>; };

// Specialize the advection type for this type tag
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::OnePTest>
{ using type = FLUXTYPE; };

} // end namespace Properties

/*!
 * \ingroup
 * \brief
 */
template <class TypeTag>
class OnePTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    // using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    enum {
        // index of the primary variable
        bothPhases = Indices::bothPhases,
        liquidPhaseOnly = Indices::liquidPhaseOnly,
        gasPhaseOnly = Indices::gasPhaseOnly,
        pressureIdx = Indices::pressureIdx,
        conti0EqIdx = Indices::conti0EqIdx
    };

    // using Intersection = typename GridView::Intersection;

public:
    OnePTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        name_ = getParam<std::string>("Problem.Name");
        atmosphericPressure_ = getParam<Scalar>("Initial.atmosphericPressure");
        radiusWell_ = getParam<Scalar>("Well.radiusWell");
        hWell_ = getParam<Scalar>("Well.hWell");
        aquiferDepth_ = getParam<Scalar>("Aquifer.aquiferDepth");
        aquiferRadius_ = getParam<Scalar>("Aquifer.aquiferRadius");
        hAquifer_ = getParam<Scalar>("Aquifer.hAquifer");
        kf_ = getParam<Scalar>("SpatialParams.kf");
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    std::string name() const
    {
        return name_;
    }

    /*!
     * \brief Returns the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    {
        return 273.15 + 20.0; // 20 °C
    }

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonWettingReferencePressure() const
    {
        return atmosphericPressure_;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        const Scalar radius = sqrt(globalPos[0] * globalPos[0] + globalPos[1] * globalPos[1]);
        if (radius < radiusWell_)
        {
            if (globalPos[2] < hAquifer_ + eps_)
            {
                bcTypes.setAllDirichlet();
            }
            else
            {
                bcTypes.setAllNeumann();
            }
        }
        else if (globalPos[1] < eps_)
        {
            bcTypes.setAllNeumann();
        }
        else if (globalPos[1] > globalPos[0] - eps_)
        {
            bcTypes.setAllNeumann();
        }
        else if (globalPos[2] < eps_)
        {
            bcTypes.setAllNeumann();
        }
        else if (globalPos[2] > aquiferDepth_ - eps_)
        {
            bcTypes.setAllNeumann();
        }
        else
        {
            bcTypes.setAllDirichlet();
        }
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        const Scalar radius = sqrt(globalPos[0] * globalPos[0] + globalPos[1] * globalPos[1]);
        const Scalar sw = 0.999999;
        using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), sw);
        if (radius < radiusWell_)
        {
            if (globalPos[2] < hWell_ + eps_)
            {
                values[pressureIdx] = nonWettingReferencePressure() + 1000.0 * 9.81 * (hWell_ - globalPos[2]) - pc;
            }
            else
            {
                values[pressureIdx] = nonWettingReferencePressure() - pc;
            }
        }
        else
        {
            values[pressureIdx] = nonWettingReferencePressure() + 1000.0 * 9.81 * (aquiferDepth_ - globalPos[2]) - pc;
        }
        values.setState(bothPhases);
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann
     *        boundary segment.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores the mass flux
     * Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        const Scalar radius = sqrt(globalPos[0] * globalPos[0] + globalPos[1] * globalPos[1]);
        const Scalar sw = 0.999999;
        using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), sw);
        const Scalar hDupuit = sqrt(aquiferDepth_ * aquiferDepth_ - (aquiferDepth_ * aquiferDepth_ - hWell_ * hWell_) * (log(aquiferRadius_) - log(radius)) / (log(aquiferRadius_) - log(radiusWell_)));
        values[pressureIdx] = nonWettingReferencePressure() + 1000.0 * 9.81 * (hDupuit - globalPos[2]) - pc;
        values.setState(bothPhases);
        return values;
    }

    // \}

private:
    std::string name_;
    Scalar atmosphericPressure_;
    Scalar radiusWell_;
    Scalar hWell_;
    Scalar aquiferDepth_;
    Scalar aquiferRadius_;
    Scalar hAquifer_;
    Scalar kf_;
    Scalar permeability_;

    static constexpr Scalar eps_ = 1.0e-6;
};

} // end namespace Dumux

#endif
