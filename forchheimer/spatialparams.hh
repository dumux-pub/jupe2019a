// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup 
 * \brief 
 */

#ifndef DUMUX_1P_TEST_SPATIALPARAMS_HH
#define DUMUX_1P_TEST_SPATIALPARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/spatialparams/fv1p.hh>
#include <dumux/material/spatialparams/gstatrandomfield.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/porousmediumflow/richards/model.hh>

namespace Dumux {

/*!
 * \ingroup 
 * \brief 
 */
template<class FVGridGeometry, class Scalar>
class OnePTestSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, OnePTestSpatialParams<FVGridGeometry, Scalar>>
{
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, OnePTestSpatialParams<FVGridGeometry, Scalar>>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using IndexSet = typename GridView::IndexSet;
    
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

public:
    using MaterialLaw = EffToAbsLaw<RegularizedVanGenuchten<Scalar>>;
    using MaterialLawParams = typename MaterialLaw::Params;
    // export permeability type
    using PermeabilityType = Scalar;
    
    OnePTestSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry),
    randomPermeability_(fvGridGeometry->gridView().size(dim), 0.0),
    indexSet_(fvGridGeometry->gridView().indexSet())
    {
        randomField_ = getParam<bool>("SpatialParams.RandomField", false);
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");
        if(!randomField_)
            permeabilityLens_ = getParam<Scalar>("SpatialParams.PermeabilityLens");
        else
            initRandomField(*fvGridGeometry);
        
        lensLowerLeft_ = getParam<GlobalPosition>("SpatialParams.LensLowerLeft");
        lensUpperRight_ = getParam<GlobalPosition>("SpatialParams.LensUpperRight");
        
        lensMaterialParams_.setSwr(0.0);
        lensMaterialParams_.setSnr(0.0);
        outerMaterialParams_.setSwr(0.0);
        outerMaterialParams_.setSnr(0.0);

        // parameters for the Van Genuchten law
        // alpha and n
        vgn_ = getParam<Scalar>("vanGenuchten.vgn");
        vgalpha_ = getParam<Scalar>("vanGenuchten.vgalpha");
        
        lensMaterialParams_.setVgAlpha(vgalpha_);
        lensMaterialParams_.setVgn(vgn_);
        outerMaterialParams_.setVgAlpha(vgalpha_);
        outerMaterialParams_.setVgn(vgn_);
        
        lensK_ = getParam<Scalar>("SpatialParams.PermeabilityLens");
        outerK_ = getParam<Scalar>("SpatialParams.Permeability");
        
        radiusWell_ = getParam<Scalar>("Well.radiusWell");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *
     * \param element The element
     * \param scv The sub-control volume
     * \param elemSol The element solution vector
     * \return The intrinsic permeability
     */
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();

        return materialLawParamsAtPos(globalPos);
    }
    
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        if (isInLens_(globalPos))
            return lensMaterialParams_;
        return outerMaterialParams_;
    }

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (isInLens_(scv.dofPosition()))
        {
            if(randomField_)
                return randomPermeability_[indexSet_.index(element)];
            else
                return permeabilityLens_;
        }
        else
            return permeability_;
    }
    
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (isInLens_(globalPos))
            return lensK_;
        return outerK_;
    }

    /*! \brief Defines the porosity in [-].
   *
   * \param globalPos The global position where we evaluate
   */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        return 0.4;
    }
    
    /*!
     * \brief This method allows the generation of a statistical field using gstat
     *
     * \param gg The finite-volume grid geometry used by the problem
     */
    void initRandomField(const FVGridGeometry& gg)
    {
        const auto& gridView = gg.gridView();
        const auto& elementMapper = gg.elementMapper();
        const auto gStatControlFile = getParam<std::string>("Gstat.ControlFile");
        const auto gStatInputFile = getParam<std::string>("Gstat.InputFile");
        const auto outputFilePrefix = getParam<std::string>("Gstat.OutputFilePrefix");

        // create random permeability object
        using RandomField = GstatRandomField<GridView, Scalar>;
        RandomField randomPermeabilityField(gridView, elementMapper);
        randomPermeabilityField.create(gStatControlFile,
                                       gStatInputFile,
                                       outputFilePrefix + ".dat",
                                       RandomField::FieldType::log10,
                                       true);
        randomPermeability_.resize(gridView.size(dim), 0.0);

        // copy vector from the temporary gstat object
        randomPermeability_ = randomPermeabilityField.data();
    }

    //! get the permeability field for output
    const std::vector<Scalar>& getPermField() const
    { return randomPermeability_; }

private:
    bool isInLens_(const GlobalPosition &globalPos) const
    {
        return false;
    }

    GlobalPosition lensLowerLeft_;
    GlobalPosition lensUpperRight_;
    
    Scalar lensK_;
    Scalar outerK_;
    MaterialLawParams lensMaterialParams_;
    MaterialLawParams outerMaterialParams_;
    
    bool randomField_;
    
    Scalar permeability_, permeabilityLens_;
    Scalar radiusWell_;
    Scalar vgn_;
    Scalar vgalpha_;
    std::vector<Scalar> randomPermeability_;

    const IndexSet& indexSet_;
    
    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace

#endif
