function Gitter_dgf_strukturiert_3D(dr, dphi, dz, Pfad)

% Erstellung eines strukturierten 3D-Gitters
% fuer einen Ausschnitt aus einem Hohlraumzylinder (Kuchenstueck)
% dr - Vektor radiale Richtung
% dphi - Vektor Oeffnungswinkel
% dz - Vektor vertikale Richtung
% Pfad - Ausgabepfad des Gitters (.dgf-Datei)

fid = fopen(Pfad,'w');
fprintf(fid, 'DGF\n');
fprintf(fid, 'Vertex\n');
n = 0;
nodeNo(1)=0;
for l=1:length(dz)
    for j=1:length(dphi)
        for i=1:length(dr)
            n = n+1;
        x = dr(i)*cos(dphi(j));
        y = dr(i)*sin(dphi(j));
            nodeNo(n) = n-1;
            fprintf(fid, '%12.15f %12.15f %12.15f \n ', x,y,dz(l));
        end
    end
end
fprintf(fid, '#\n');

fprintf(fid, 'CUBE\n');
n = 0; 
for l=1:length(dz) -1
    for j=1:length(dphi) -1
        for i=1:length(dr) -1
            n = n+1;
            fprintf(fid, '%d %d %d %d %d %d %d %d\n', nodeNo(n), nodeNo(n+1), nodeNo(n+length(dr)), nodeNo(n+length(dr)+1), nodeNo(n+length(dr)*length(dphi)), nodeNo(n+length(dr)*length(dphi)+1), nodeNo(n+length(dr)*length(dphi)+length(dr)), nodeNo(n+length(dr)*length(dphi)+length(dr)+1));
        end
        n=n+1;
    end
    n=n+length(dr);
end

fprintf(fid, '#\n');
fprintf(fid, 'BOUNDARYDOMAIN\n');
fprintf(fid, 'default 1\n');
fprintf(fid, '#\n');
fprintf('Done \n');

end
